﻿using UnityEngine;
using System.Collections;

public class EnemyScr : MonoBehaviour {
	private Vector2[] path;
	private float offsetx = 6.0f, offsety = 4.0f, tileSize = 40.0f;
	private int goalTile = 0;
	private Vector2 position, goal;
	private float speed = Time.deltaTime * 10.0f;
    public int health;

    private int offsetForMap = -40;

    GameObject go;
	// Use this for initialization
	private bool finished = false;

	public void construct (Vector2[] path, Vector2 startPos) {
		this.path = path;
		this.go = gameObject;
		this.position = startPos;
		this.goal = new Vector2 ((this.path[goalTile].x - offsetx) * (this.tileSize/2), (this.path[goalTile].y) * (this.tileSize/2) + offsetForMap);
		begin();
	}

	private void setGoal(int tile) {
		this.goal = new Vector2 ((this.path[tile].x - offsetx) * (this.tileSize/2), (this.path[tile].y) * (this.tileSize/2) + offsetForMap);
	}

    public void setHealth(int health)
    {
        this.health = health;
    }

    public int getHealth()
    {
        return this.health;
    }

    public void hit(int hit)
    {
        this.health -= hit;
    }

    public bool isAlive()
    {
        return (health > 0);
    }

	public void begin() {
		this.go.transform.position = new Vector3 ( position.x , 0, position.y);
		this.go.transform.LookAt( new Vector3(this.goal.x, 0, this.goal.y) );
		if (this.position.x > this.goal.x) {
			if (this.position.x - this.goal.x < this.speed) {
				this.position.x = this.goal.x;
			} else {
				this.position.x -= this.speed;
			}
		}

		if (this.position.x < this.goal.x) {
			if (this.goal.x - this.position.x < this.speed) {
				this.position.x = this.goal.x;
			} else {
				this.position.x += this.speed;
			}
		}
		if (this.position.y > this.goal.y) {
			if (this.position.y - this.goal.y < this.speed) {
				this.position.y = this.goal.y;
			} else {
				this.position.y -= this.speed;
			}
		}

		if (this.position.y < this.goal.y) {
			if (this.goal.y - this.position.y < this.speed) {
				this.position.y = this.goal.y;
			} else {
				this.position.y += this.speed;
			}
		}

		if (this.position.y == this.goal.y && this.position.x == this.goal.x) {
			this.goalTile++;
			if (goalTile < this.path.Length)
				setGoal (this.goalTile);
			else
				victory ();
		}
	}

	private void victory() {
		this.finished = true;
	}

	public bool hasFinished() {
		return this.finished;
	}
}
