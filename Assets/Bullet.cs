﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

    private Vector3 start, end;

	public void setVars (Vector3 s, Vector3 e)
    {
        this.start = s;
        this.end = e;
    }

    public void drawShootLine(Vector3 start, Vector3 end)
    {

        StartCoroutine(drawLine(start, end, Color.cyan, 0.2f));

    }


    IEnumerator drawLine(Vector3 start, Vector3 end, Color color, float duration = 0.2f)
    {
        GameObject myLine = new GameObject();
        myLine.transform.position = start;
        myLine.AddComponent<LineRenderer>();
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Particles/Additive"));
        lr.SetColors(color, color);
        lr.SetWidth(0.5f, 0.5f);
        lr.SetPosition(0, start);
        lr.SetPosition(1, end);
        yield return new WaitForSeconds(duration);
        GameObject.Destroy(myLine);
        GameObject.Destroy(this.gameObject);
    }
}
