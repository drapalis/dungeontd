﻿using UnityEngine;
using System.Collections;
using Vuforia;

public class MenuScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		RaycastHit hit = new RaycastHit ();
		Ray cameraRay = Camera.main.ScreenPointToRay (Input.mousePosition);
		if (Input.touchCount > 0 || Input.GetMouseButtonDown(0)) { 
			if (Physics.Raycast(cameraRay, out hit)) {
				if (hit.collider.tag == "Exit") {
					Application.Quit();
				}
				if (hit.collider.tag == "Start") {
					Application.LoadLevel(1);
				}
			}
		}

		CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
	}
}
