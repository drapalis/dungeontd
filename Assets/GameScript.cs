﻿using UnityEngine;
using System.Collections;
using Vuforia;

public class GameScript : MonoBehaviour {
	
	private int[,] track = new int[,] {
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1},
		{0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
	};

    public AudioClip shoot, enemyWentThrough;
    private AudioSource source;
    private float volume = 1.0f;

	private int trackx = 15, tracky = 5;
    private int offsetForMap = -40;
	public bool manualStart = false;
	private Vector2[] path = new Vector2[] {
		new Vector2(0,2),
		new Vector2(1,2),
		new Vector2(2,2),
		new Vector2(3,2),
		new Vector2(3,3),
		new Vector2(4,3),
		new Vector2(5,3),
		new Vector2(6,3),
		new Vector2(6,2),
		new Vector2(6,1),
		new Vector2(7,1),
		new Vector2(8,1),
		new Vector2(9,1),
		new Vector2(10,1),
		new Vector2(10,2),
		new Vector2(11,2),
		new Vector2(12,2),
		new Vector2(13,2),
		new Vector2(14,2)
	};
	public bool canStart = true, gameStarted = false;
	private int wave = -1;

	private int hp = 100, cash = 50;

	private int[,] waves = new int[,] {
		{20, 50},
		{25, 50},
		{25, 60},
		{20, 80},
		{10, 100}
	};

	public GameObject[] enemies, towers;
	private int towersCount = 0, towersBuilt = 0;
	public GameObject go;
	public GameObject hud;
	bool hasTorch;
	// Use this for initialization
	void Start () {

        source = GetComponent<AudioSource> ();

		for (int i = 0; i < trackx; i++) {
			for (int o = 0; o < tracky; o++) {
				GameObject field = Instantiate(Resources.Load("MiddleFIeld"), new Vector3((i-6)*20,0,(4-o)*20 + offsetForMap),  new Quaternion(0,0,0,0)) as GameObject;
				field.transform.parent = go.transform;
                GameObject select = Instantiate(Resources.Load("TowerSelectC"), new Vector3((i - 6) * 20, 0, (4 - o) * 20 + offsetForMap), new Quaternion(0, 180, 0, 0)) as GameObject;
                select.transform.parent = field.transform;
                select.active = false;
                if (track [o, i] == 1) {
					field.GetComponent<Field> ().setTrack ();
				} else {
					towersCount++;
				}
			}
		}
		towers = new GameObject[towersCount];
	}

	private void startGame(int level) {
		foreach (GameObject enemy in enemies) {
			if (enemy != null) {
				Destroy (enemy);
			}
		}
		enemies = new GameObject[this.waves[level,0]];
		for(int i = 0; i < this.waves[level,0]; i++){
			Vector2 startPos = new Vector2 ((this.path[0].x-6)*20-i*8,(4-this.path[0].y)* 20 + offsetForMap);
			enemies[i] = Instantiate (Resources.Load ("goblin"), new Vector3(startPos.x,0,startPos.y),  new Quaternion(0,0,0,0)) as GameObject;
			enemies[i].transform.parent = go.transform;
			enemies[i].transform.tag = "Enemy";
            enemies[i].GetComponent<EnemyScr>().construct(this.path, startPos);
            enemies[i].GetComponent<EnemyScr>().setHealth(this.waves[level,1]);
        }
		this.wave++;
		this.gameStarted = true;
		this.canStart = false;
	}

	private void moveEnemies() {
		if (this.gameStarted) {
			int alive = 0;
			for (int i = 0; i < enemies.Length; i++) {
				if (enemies [i] != null) {
					if (enemies [i].GetComponent<EnemyScr> ().hasFinished ()) {
						Destroy (enemies [i]);
						this.hp--;
                        source.PlayOneShot(enemyWentThrough, 1.0f);
					} else {
						enemies [i].GetComponent<EnemyScr> ().begin ();
						alive++; 
					}
				}
			}
			if (alive < 1) {
				this.canStart = true;
				this.gameStarted = false;
			}
		}
	}
	// Update is called once per frame
	void Update () {
		hasTorch=CameraDevice.Instance.SetFlashTorchMode(true);
        
        RaycastHit hit = new RaycastHit ();
		Ray cameraRay = Camera.main.ScreenPointToRay (Input.mousePosition);
        for (int i = 0; i < towers.Length; i++)
        {
            if (towers[i] != null)
            {
                for (int j = 0; j < enemies.Length; j++)
                {
                    if (enemies[j] != null) 
                    {
                        if (towers[i].GetComponent<TowerManager>().canShoot())
                        {
                            if (Vector3.Distance(towers[i].transform.GetChild(0).position, enemies[j].transform.position) < towers[i].GetComponent<TowerManager>().getRange())
                            {
                                //Debug.DrawLine(towers[i].transform.GetChild(1).position, enemies[j].transform.position, Color.cyan);
                                towers[i].GetComponent<TowerManager>().shoot();
                                enemies[j].GetComponent<EnemyScr>().hit(towers[i].GetComponent<TowerManager>().getDamage());
                                if (!enemies[j].GetComponent<EnemyScr>().isAlive())
                                {
                                    this.cash++;
                                    Destroy(enemies[j].gameObject);
                                }
                                GameObject bullet = Instantiate(Resources.Load("Sphere"), towers[i].gameObject.transform.position, new Quaternion(0,0,0,0)) as GameObject;
                                bullet.GetComponent<Bullet>().drawShootLine(towers[i].transform.GetChild(1).position, enemies[j].transform.position);
                                source.pitch = 0.5f * towers[i].GetComponent<TowerManager>().getTowerType() + 0.5f;
                                source.PlayOneShot(shoot, volume);
                            }
                        }
                    }
                }
                towers[i].GetComponent<TowerManager>().addToTimer(Time.deltaTime);
            }
        }

		if (Input.GetMouseButtonDown(0)) { 
			if (Physics.Raycast(cameraRay, out hit)) {
				if (hit.collider.name == "X") {
					Application.LoadLevel(0);
				}
				if (hit.collider.tag == "Play") {
					if(this.canStart)
						startGame (this.wave+1);
				}
				if (hit.collider.tag == "MapTile") {
					if (hit.collider.gameObject.GetComponent<Field> ().isTrack()) {
						
					} else {
                        foreach (Transform child in go.transform)
                        {
                            if (child.name == "MiddleField(Clone)")
                            {
                                if (!child.GetComponent<Field>().isTrack())
                                {
                                    child.GetChild(0).gameObject.active = false;
                                }
                            }
                        }
                        if(this.cash >= 10)
                            hit.collider.transform.GetChild(0).gameObject.active = true;
                        //towers [towersBuilt] = Instantiate (Resources.Load ("Tower"), new Vector3 (hit.collider.gameObject.transform.position.x, 0, hit.collider.gameObject.transform.position.z), new Quaternion (0, 0, 0, 0)) as GameObject; 
						//towers [towersBuilt++].transform.parent = go.transform;
						//Destroy (hit.collider.gameObject);
					}
                }
                if (hit.collider.tag == "GroundTower" && this.cash >= 10)
                {
                    towers[towersBuilt] = Instantiate(Resources.Load("Tower"), new Vector3(hit.collider.gameObject.transform.parent.parent.position.x, 0, hit.collider.gameObject.transform.parent.parent.position.z), new Quaternion(0, 0, 0, 0)) as GameObject;
                    towers[towersBuilt].GetComponent<TowerManager>().setType(1);
                    towers[towersBuilt].GetComponent<TowerManager>().setTimeout(1.5);
                    towers[towersBuilt].GetComponent<TowerManager>().setDamage(10);
                    towers[towersBuilt].GetComponent<TowerManager>().setRange(55);
                    towers[towersBuilt++].transform.parent = go.transform;
                    Destroy(hit.collider.gameObject.transform.parent.parent.gameObject);
                    this.cash -= 10;
                }
                if (hit.collider.tag == "MultiTower" && this.cash >= 10)
                {
                    towers[towersBuilt] = Instantiate(Resources.Load("Bunker"), new Vector3(hit.collider.gameObject.transform.parent.parent.position.x, 8, hit.collider.gameObject.transform.parent.parent.position.z), new Quaternion(0, 0, 0, 0)) as GameObject;
                    towers[towersBuilt].GetComponent<TowerManager>().setType(2);
                    towers[towersBuilt].GetComponent<TowerManager>().setTimeout(1);
                    towers[towersBuilt].GetComponent<TowerManager>().setDamage(5);
                    towers[towersBuilt].GetComponent<TowerManager>().setRange(65);
                    towers[towersBuilt++].transform.parent = go.transform;
                    Destroy(hit.collider.gameObject.transform.parent.parent.gameObject);
                    this.cash -= 10;
                }
                if (hit.collider.tag == "AirTower" && this.cash >= 10)
                {
                    towers[towersBuilt] = Instantiate(Resources.Load("Watch Tower/Tower2"), new Vector3(hit.collider.gameObject.transform.parent.parent.position.x+3, 23, hit.collider.gameObject.transform.parent.parent.position.z+20), new Quaternion(0, 0, 0, 0)) as GameObject;
                    towers[towersBuilt].GetComponent<TowerManager>().setType(3);
                    towers[towersBuilt].GetComponent<TowerManager>().setTimeout(1.5);
                    towers[towersBuilt].GetComponent<TowerManager>().setDamage(10);
                    towers[towersBuilt].GetComponent<TowerManager>().setRange(55);
                    towers[towersBuilt++].transform.parent = go.transform;
                    Destroy(hit.collider.gameObject.transform.parent.parent.gameObject);
                    this.cash -= 10;
                }
            }
		}
		hud.GetComponent<TextMesh> ().text = "Wave: " + ( this.wave+1 ) + "/" + this.waves.GetLength(0) + "   HP: " + this.hp + "   Cash: $" + this.cash;
		CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
		moveEnemies ();
		if(this.manualStart){
			if(this.canStart)
				startGame (this.wave+1);
			this.manualStart = false;
		}

		if (this.wave + 1 == this.waves.GetLength (0) && this.canStart && !this.gameStarted) {
			Application.LoadLevel (2);
		}

		if (this.hp < 1) {
			Application.LoadLevel (3);
		}

	}
}
