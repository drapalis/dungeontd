﻿using UnityEngine;
using System.Collections;

public class TowerManager : MonoBehaviour {

    private int type;
    private int damage;
    private double timer, timeout, range;


	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void setRange(double range)
    {
        this.range = range;
    }

    public double getRange()
    {
        return this.range;
    }

    public void setType(int type)
    {
        this.type = type;
    }

    public int getTowerType()
    {
        return this.type;
    }

    public void setDamage(int damage)
    {
        this.damage = damage;
    }

    public int getDamage()
    {
        return this.damage;
    }

    public void setTimeout(double timeout)
    {
        this.timeout = timeout;
    }

    public void addToTimer(double time)
    {
        timer += time;
    }

    public bool canShoot()
    {
        return (timer > timeout);
    }

    public void shoot()
    {
        timer = 0;
    }
}
