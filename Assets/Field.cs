﻿using UnityEngine;
using System.Collections;

public class Field : MonoBehaviour {
	public static Field instance;
	public bool isEnabled = true, Track;
	void Start(){
		instance = this;
		GetComponent<MeshRenderer> ().enabled = isEnabled;
	}

	void Update() {
        if (!this.isTrack())
            this.isEnabled = true;
        else
            this.isEnabled = false;
		GetComponent<MeshRenderer> ().enabled = isEnabled;
	}

	public void setTrack() {
		this.Track = true;
		GetComponent<MeshRenderer> ().material = Resources.Load ("TrackField") as Material;
        this.isEnabled = false;
	}
	public bool isTrack() {
		return this.Track;
	}

}
