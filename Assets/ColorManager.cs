﻿using UnityEngine;
using System.Collections;

public class ColorManager : MonoBehaviour
{
    public static ColorManager color;
    public GameObject go;
    // Use this for initialization
    void Start()
    {
        color = this;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Change()
    {
        go.transform.position += new Vector3(1.0f, 0, 0);
    }
}
